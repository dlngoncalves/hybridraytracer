#version 430
//#extension GL_ARB_shader_storage_buffer_object : enable;

//in vec2 textureCoordinate;
//in vec3 normal;
//in vec3 position;
//layout (location = 0) out vec4 out_Color;
//layout (location = 1) out vec3 out_Normal;
//layout (location = 2) out vec3 out_Position;

in vec4 textureCoordinate;
in vec4 normal;
in vec4 position;
//in int textureID;
layout (location = 0) out vec4 out_Color;
layout (location = 1) out vec4 out_Normal;
layout (location = 2) out vec4 out_Position;

//layout (binding = 0) uniform sampler2D curTexture;
layout (binding = 0) uniform sampler2DArray curTexture;

void main(void)
{
	//float id = textureID;
	out_Normal = normal;
	out_Position = position;   
	//out_Color = texture(curTexture,vec3(textureCoordinate.s,textureCoordinate.t,0.0));  
	out_Color = texture(curTexture,vec3(textureCoordinate));  
	out_Color.a = textureCoordinate.p;
	//glsl playground starts here
	//out_Color = vec4(1.0,1.0,0.0,1.0);
	//float attn = 0.0;

	//if(gl_FragCoord.w < 0.01)		
	//	discard;
	//if(gl_FragCoord.w < 0.1)		
	//	attn = 0.1;
	//else if(gl_FragCoord.w < 0.3)		
	//	attn = 0.3;
	//else if(gl_FragCoord.w < 0.5)		
	//	attn = 0.5;
	//else if(gl_FragCoord.w < 0.7)		
	//	attn = 0.7;
	//else if(gl_FragCoord.w < 0.9)		
	//	attn = 0.9;
	//else if(gl_FragCoord.w >= 0.9)		
	//	attn = 1.0;

	//out_Color *= attn;
	//out_Color = vec4(gl_FragCoord.w,0.0,0.0,1.0);	
	//out_Color = vec4(position.x,position.y,position.z,1.0);
}