#pragma once

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

using namespace std;

struct Vertex{
	glm::vec3 position;	
	glm::vec3 normal;	
	glm::vec3 texture;	
};

struct Texture {
	GLuint id;
	string type;
};

class Mesh {
public:
	/*  Mesh Data  */
	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;
	/*  Functions  */
	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures);
	//void Draw(Shader shader);
	void Draw();//no shader class for now
private:
	/*  Render data  */
	GLuint VAO, VBO, EBO;
	/*  Functions    */
	void setupMesh();
};

