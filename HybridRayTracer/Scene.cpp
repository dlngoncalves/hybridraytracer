#define GLM_SWIZZLE 
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Scene.h"
#include "GLSLShader.h"
#include <iostream>
#include <fstream> 
#include <sstream> 
#include <string>

Scene* Scene::_instance = nullptr;

Scene* Scene::instance()
{
	if (!_instance)
		_instance = new Scene;
	return _instance;
}

void Scene::loadScene(const std::string &sceneFile, GLSLShader &baseShader)//assumes every object shares a shader (because they do)
{
	sceneName = sceneFile;
	
	//ifstream in(filename, ios::in);
	
	ifstream in;

	in.open(sceneFile, ios::in);
	if (!in)//BETTER ERROR HANDLING!!! -DIEGO
	{
		cerr << "Cannot open " << sceneFile << endl; exit(1);
	}

	string line;

	while (getline(in, line))
	{
		if (line[0] == '#' || line[0] == '/')
			continue;

		if (line.substr(0, 1) == "o"){
			istringstream s(line.substr(1));
			string name;
			GLfloat reflection;
			glm::vec3 position;
			s >> name; s >> position.x; s >> position.y; s >> position.z;
			s >> reflection;			
			Object obj(name, baseShader,reflection);
			obj.modelMatrix = glm::translate(glm::mat4(1.0), position);
			obj.reflection = reflection;
			objects.push_back(obj);			
		}

		if (line.substr(0, 1) == "l"){
			istringstream s(line.substr(1));
			Light newLight;
			s >> newLight.position.x; s >> newLight.position.y; s >> newLight.position.z;
			s >> newLight.specular.r; s >> newLight.specular.g; s >> newLight.specular.b;
			s >> newLight.diffuse.r; s >> newLight.diffuse.g; s >> newLight.diffuse.b;
			s >> newLight.ambient.r; s >> newLight.ambient.g; s >> newLight.ambient.b;
			lights.push_back(newLight);
		}

	}
	setupBFVertexDataBuffer();	
	Object::loadTextureGlobal(objects);
}

void Scene::setupBFVertexDataBuffer()
{
	int index = 0;
	for (auto object : Scene::instance()->objects){		
		BFVertexDataBuffer.insert(BFVertexDataBuffer.end(), object.vertexData.begin(), object.vertexData.end());
		for (auto vertex : object.vertexData){
			glm::vec3 vertTest = (object.modelMatrix * glm::vec4(BFVertexDataBuffer[index].position, 1.0)).xyz();
			BFVertexDataBuffer[index].position = vertTest;
			index++;
		}
	}
}

Scene::Scene()
{
}


Scene::~Scene()
{
}