#define GLM_SWIZZLE 
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "gl_utils.h"
#define GLEW_STATIC
#include <GL/glew.h> // include GLEW and new version of GL on Windows
#include <GLFW/glfw3.h> // GLFW helper library
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#define _USE_MATH_DEFINES
#include <math.h>
#define GL_LOG_FILE "gl.log"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "GLSLShader.h"
#include "Object.h"
#include "Camera.h"
#include "Scene.h"
#include "Light.h"
using namespace std;

int g_gl_width = 800;
int g_gl_height = 500;

GLFWwindow* g_window = NULL;

Camera camera;
GLfloat lastx = g_gl_width/2, lasty = g_gl_height/2;
bool firstMouse = true;

GLuint frameBuffer;
GLuint frameBufferTexture;
GLuint framBufferNormalTexture;
GLuint frameBufferPositionTexture;

GLuint depthTexture;
GLuint depthrenderbuffer;

GLuint firstPassTexture;
GLuint firstTexture;
GLuint secondTexture;

GLint viewMatrixLocation;
GLint perspectiveMatrixLocation;
GLint modelMatrixLocation;

GLuint secondPassVao;
GLuint secondPassShaderProgram;
GLuint secondPassTimeLocation;
GLuint secondPassModeLocation;

GLuint posSSbo;
GLuint lightSSbo;
GLuint materialSSbo;

//vector<vertex> BFVertexDataBuffer;
//vector<Object> objects;

//full screen quad declared here
GLfloat points[] = {
	-1.0, -1.0,
	1.0, -1.0,
	1.0, 1.0,
	1.0, 1.0,
	-1.0, 1.0,
	-1.0, -1.0
};

void createFramebuffer()
{
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(1, &frameBufferTexture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, frameBufferTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, g_gl_width, g_gl_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture, 0);

	glGenTextures(1, &framBufferNormalTexture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, framBufferNormalTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, g_gl_width, g_gl_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, framBufferNormalTexture, 0);


	glGenTextures(1, &frameBufferPositionTexture);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, frameBufferPositionTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, g_gl_width, g_gl_height, 0, GL_RGB, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, frameBufferPositionTexture, 0);


	glGenTextures(1, &depthTexture);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, g_gl_width, g_gl_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);


	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, g_gl_width, g_gl_height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	GLenum drawBuffs[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};	
	glDrawBuffers(3, drawBuffs);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//what each texture needs as unique identifier?
//just the GLuint texture ID?
bool load_texture(const char* file_name, GLuint* tex) {
	int x, y, n;
	int force_channels = 4;
	unsigned char* image_data = stbi_load(file_name, &x, &y, &n, force_channels);
	if (!image_data) {
		fprintf(stderr, "ERROR: could not load %s\n", file_name);
		return false;
	}
	// NPOT check
	if ((x & (x - 1)) != 0 || (y & (y - 1)) != 0) {
		fprintf(
			stderr, "WARNING: texture %s is not power-of-2 dimensions\n", file_name
			);
	}
	int width_in_bytes = x * 4;
	unsigned char *top = NULL;
	unsigned char *bottom = NULL;
	unsigned char temp = 0;
	int half_height = y / 2;

	for (int row = 0; row < half_height; row++) {
		top = image_data + row * width_in_bytes;
		bottom = image_data + (y - row - 1) * width_in_bytes;
		for (int col = 0; col < width_in_bytes; col++) {
			temp = *top;
			*top = *bottom;
			*bottom = temp;
			top++;
			bottom++;
		}
	}
	glGenTextures(1, tex);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, *tex);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RGBA,
		x,
		y,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		image_data
		);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	
	//GLfloat max_aniso = 0.0f;
	//glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_aniso);
	// set the maximum!
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_aniso);
	return true;
}


void loadTexture(const char* filename){
	int x, y, n;
	int force_channels = 4;
	unsigned char* image_data = stbi_load(filename, &x, &y, &n, force_channels);


	glGenTextures(1, &firstPassTexture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, firstPassTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);

	glGenerateMipmap(GL_TEXTURE_2D);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	

	GLfloat maxAniso = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);
}

void load_mtl(const char* filename )
{
	//ifstream in(filename, ios::in);
	material baseMaterial;
	ifstream in;

	in.open(filename, ios::in);
	if (!in)
	{
		cerr << "Cannot open " << filename << endl; exit(1);
	}

	string line;

	while (getline(in, line))
	{
		if (line[0] == '#')
			continue;

		if (line.substr(0, 2) == "Ka"){
			istringstream s(line.substr(2));
			s >> baseMaterial.ambient.r; s >> baseMaterial.ambient.g; s >> baseMaterial.ambient.b;
		}

		if (line.substr(0, 2) == "Kd"){
			istringstream s(line.substr(2));
			s >> baseMaterial.diffuse.r; s >> baseMaterial.diffuse.g; s >> baseMaterial.diffuse.b;
		}

		if (line.substr(0, 2) == "Ks"){
			istringstream s(line.substr(2));
			s >> baseMaterial.specular.r; s >> baseMaterial.specular.g; s >> baseMaterial.specular.b;
		}

		if (line.substr(0, 2) == "Ns"){
			istringstream s(line.substr(2));
			s >> baseMaterial.specTranspText.x;
		}

		if (line[0] == 'd'){
			istringstream s(line.substr(1));
			s >> baseMaterial.specTranspText.y;
		}

		if (line.substr(0, 3) == "map"){
			//do something here -DIEGO
			baseMaterial.specTranspText.z = 1;// line.substr(7);
		}

	}
	//Object::materialList[filename] = baseMaterial;
	//Object::materialList.insert(std::make_pair(filename, baseMaterial)); alternative syntax
	
	//carregar os dados do material do obj e passar para o shader. salvar em uma textura um id daquele material.
}

void load_obj(const char* filename, vector<vertex> &vertexData)
{
	vector<glm::vec3> tempVertices;
	vector<glm::vec3> tempNormals;
	vector<glm::vec3> tempTextures;
	vector<face> tempFaces;

	ifstream in(filename, ios::in);
	if (!in)
	{
		cerr << "Cannot open " << filename << endl; exit(1);
	}

	string line;
	while (getline(in, line))
	{
		if (line.substr(0, 2) == "v ")
		{
			istringstream s(line.substr(2));
			glm::vec3 v; s >> v.x; s >> v.y; s >> v.z;// v.w = 1.0f;
			tempVertices.push_back(v);
		}
		else if (line.substr(0, 2) == "vn"){
			istringstream s(line.substr(3));
			glm::vec3 n; s >> n.x; s >> n.y; s >> n.z;
			tempNormals.push_back(n);
		}
		else if (line.substr(0, 2) == "vt"){
			istringstream s(line.substr(3));
			glm::vec3 t; s >> t.s; s >> t.t;
			tempTextures.push_back(t);
		}

		else if (line.substr(0, 2) == "f ")
		{
			istringstream s(line.substr(2));
			face newFace;

			s >> newFace.vertexData[0] >> newFace.vertexData[1] >> newFace.vertexData[2];
			tempFaces.push_back(newFace);

		}
		else if (line[0] == '#')
		{
			/* ignoring this line */
		}
		else
		{
			/* ignoring this line */
		}
	}

	for (auto curFace : tempFaces){
		vertex newVertex;

		for (int i = 0; i < 3; i++){
			int firstSlash = curFace.vertexData[i].find_first_of("/");
			int secondSlash = curFace.vertexData[i].find_last_of("/");

			int vertexIndex = stoi(curFace.vertexData[i].substr(0, firstSlash));
			newVertex.position = tempVertices[vertexIndex - 1];

			if (tempTextures.size() > 0){
				//int textureIndex = stoi(curFace.vertexData[i].substr(firstSlash+1, secondSlash-2));
				//int textureIndex =  stoi(curFace.vertexData[i].substr(0, firstSlash));
				int textureIndex = stoi(curFace.vertexData[i].substr(firstSlash + 1, secondSlash - firstSlash-1));
				newVertex.texture = tempTextures[textureIndex - 1];
			}

			if (tempNormals.size() > 0){
				int normalIndex = stoi(curFace.vertexData[i].substr(secondSlash + 1));
				newVertex.normal = tempNormals[normalIndex - 1];
			}

			vertexData.push_back(newVertex);
		}
	}

}

/*
void loadFirstPassData(const vector<vertex> &data)
{
	//the vertex buffer is part of the object
	GLuint vertexDataBuffer;
	glGenBuffers(1, &vertexDataBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexDataBuffer);
	glBufferData(GL_ARRAY_BUFFER, data.size() * ((3 * sizeof(glm::vec3)) + 3 * sizeof(float)), data.data(), GL_STATIC_DRAW);


	glGenVertexArrays(1, &firstPassVao);
	glBindVertexArray(firstPassVao);
	glBindBuffer(GL_ARRAY_BUFFER, vertexDataBuffer);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), (GLvoid *)(sizeof(glm::vec3) + sizeof(float)));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), (GLvoid *)(2 * (sizeof(glm::vec3) + sizeof(float))));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}*/

//even easier to refactor!
void loadSecondPassData(){

	float pointCount = 6;
	GLuint points_vbo;
	glGenBuffers(1, &points_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
	glBufferData(GL_ARRAY_BUFFER, 3 * pointCount * sizeof (GLfloat), points, GL_STATIC_DRAW);

	glGenVertexArrays(1, &secondPassVao);
	glBindVertexArray(secondPassVao);
	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
}
/*
void createFirstPassShaders()
{
	char vertex_shader[1024 * 256];
	char fragment_shader[1024 * 256];

	assert(parse_file_into_str("firstPassVS.glsl", vertex_shader, 1024 * 256));
	assert(parse_file_into_str("firstPassFS.glsl", fragment_shader, 1024 * 256));

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* p = (const GLchar*)vertex_shader;
	glShaderSource(vs, 1, &p, NULL);
	glCompileShader(vs);

	// check for compile errors
	int params = -1;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &params);
	if (GL_TRUE != params) {
		fprintf(stderr, "ERROR: GL shader index %i did not compile\n", vs);
		print_shader_info_log(vs);
		return;// 1; // or exit or something
	}

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	p = (const GLchar*)fragment_shader;
	glShaderSource(fs, 1, &p, NULL);
	glCompileShader(fs);

	// check for compile errors
	glGetShaderiv(fs, GL_COMPILE_STATUS, &params);
	if (GL_TRUE != params) {
		fprintf(stderr, "ERROR: GL shader index %i did not compile\n", fs);
		print_shader_info_log(fs);
		return;// 1; // or exit or something
	}

	firstPassShaderProgram = glCreateProgram();
	glAttachShader(firstPassShaderProgram, fs);
	glAttachShader(firstPassShaderProgram, vs);

	glLinkProgram(firstPassShaderProgram);

	glGetProgramiv(firstPassShaderProgram, GL_LINK_STATUS, &params);
	if (GL_TRUE != params) {
		fprintf(
			stderr,
			"ERROR: could not link shader programme GL index %i\n",
			firstPassShaderProgram
			);
		print_programme_info_log(firstPassShaderProgram);
		return;// false;
	}

	glUseProgram(firstPassShaderProgram);//precisa do useProgram nesta fun��o para setar os uniforms

	glm::mat4 viewMatrix = glm::lookAt(camera.cameraPosition,camera.cameraPosition + camera.cameraFront,camera.cameraUp);
	glm::mat4 perspectiveMatrix = glm::perspective(glm::radians(45.0f), (float)g_gl_width / g_gl_height, 0.1f, 100.0f);
	glm::mat4 modelMatrix = glm::mat4(1.0f);
	//modelMatrix = glm::translate(modelMatrix, glm::vec3(2.0, 0.0, 5.0));
	viewMatrixLocation = glGetUniformLocation(firstPassShaderProgram, "viewMatrix");
	perspectiveMatrixLocation = glGetUniformLocation(firstPassShaderProgram, "perspectiveMatrix");
	modelMatrixLocation = glGetUniformLocation(firstPassShaderProgram, "modelMatrix");

	glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(perspectiveMatrixLocation, 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));
	glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix));
}
*/
void createSecondPassShaders()
{
	char vertex_shader[1024 * 256];
	char fragment_shader[1024 * 256];

	assert(parse_file_into_str("secondPassVS.glsl", vertex_shader, 1024 * 256));
	//assert(parse_file_into_str("secondPassFS1.glsl", fragment_shader, 1024 * 256));
	assert(parse_file_into_str("secondPassFS.glsl", fragment_shader, 1024 * 256));

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* p = (const GLchar*)vertex_shader;
	glShaderSource(vs, 1, &p, NULL);
	glCompileShader(vs);

	// check for compile errors
	int params = -1;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &params);
	if (GL_TRUE != params) {
		fprintf(stderr, "ERROR: GL shader index %i did not compile\n", vs);
		print_shader_info_log(vs);
		return;// 1; // or exit or something
	}

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	p = (const GLchar*)fragment_shader;
	glShaderSource(fs, 1, &p, NULL);
	glCompileShader(fs);

	// check for compile errors
	glGetShaderiv(fs, GL_COMPILE_STATUS, &params);
	if (GL_TRUE != params) {
		fprintf(stderr, "ERROR: GL shader index %i did not compile\n", fs);
		print_shader_info_log(fs);
		return;// 1; // or exit or something
	}

	secondPassShaderProgram = glCreateProgram();
	glAttachShader(secondPassShaderProgram, fs);
	glAttachShader(secondPassShaderProgram, vs);

	glLinkProgram(secondPassShaderProgram);

	glUseProgram(secondPassShaderProgram);

	secondPassTimeLocation = glGetUniformLocation(secondPassShaderProgram, "time");	
	secondPassModeLocation = glGetUniformLocation(secondPassShaderProgram, "mode");
	glUniform1i(secondPassTimeLocation, 0);
	glUniform1i(secondPassModeLocation, 2);

	GLint width = glGetUniformLocation(secondPassShaderProgram, "width");
	GLint height = glGetUniformLocation(secondPassShaderProgram, "height");

	glUniform1i(width, g_gl_width);
	glUniform1i(height, g_gl_height);

	GLuint camPosLoc = glGetUniformLocation(secondPassShaderProgram,"cameraPosition");
	glUniform3f(camPosLoc, camera.cameraPosition.x, camera.cameraPosition.y, camera.cameraPosition.z);

	//most complex change will probably be here
	//First, instead of just passing the global vertexData buffer, we will need to create a BIG buffer with all the data(or pass each buffer)
	//also, this buffer will HAVE to take into account the model matrix of each object - otherwise world space ray tracing wont work
	//if objects are moving around the scene (a good demo perhaps) this will need to happen EVERY FRAME! - costly maybe?
	//maybe do things in model space-translate ray to model space? or just send the matrix and multiply in the shader?

	glGenBuffers(1, &posSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, posSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, (sizeof(vertex)) * Scene::instance()->BFVertexDataBuffer.size(), NULL, GL_DYNAMIC_COPY);
	GLint bufMask = GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT;

	vertex *mapped = reinterpret_cast<vertex *>(glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, (sizeof(vertex)) * Scene::instance()->BFVertexDataBuffer.size(), bufMask));
	std::copy(Scene::instance()->BFVertexDataBuffer.begin(), Scene::instance()->BFVertexDataBuffer.end(), mapped);

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, posSSbo);
	
	glGenBuffers(1, &lightSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, (sizeof(Light)) * Scene::instance()->lights.size(), NULL, GL_STATIC_COPY);
	//GLint bufMask = GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT;

	Light *mappedLight = reinterpret_cast<Light *>(glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, (sizeof(Light)) * Scene::instance()->lights.size(), bufMask));
	std::copy(Scene::instance()->lights.begin(), Scene::instance()->lights.end(), mappedLight);

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, lightSSbo);

	glGenBuffers(1, &materialSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, materialSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, (sizeof(material)) * Object::materialList.size(), NULL, GL_STATIC_COPY);
	//GLint bufMask = GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT;

	material *mappedMaterial = reinterpret_cast<material *>(glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, (sizeof(material)) * Object::materialList.size(), bufMask));
	std::copy(Object::materialList.begin(), Object::materialList.end(), mappedMaterial);

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, materialSSbo);

	glGetProgramiv(secondPassShaderProgram, GL_LINK_STATUS, &params);

	if (GL_TRUE != params) {
		fprintf(
			stderr,
			"ERROR: could not link shader programme GL index %i\n",
			secondPassShaderProgram
			);
		print_programme_info_log(secondPassShaderProgram);
		return;// false;
	}
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastx = xpos;
		lasty = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastx;
	GLfloat yoffset = lasty - ypos; // Reversed since y-coordinates range from bottom to top
	lastx = xpos;
	lasty = ypos;

	GLfloat sensitivity = 0.05f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	camera.pitch += yoffset;
	camera.yaw += xoffset;

	if (camera.pitch > 89.0f)
		camera.pitch = 89.0f;
	if (camera.pitch < -89.0f)
		camera.pitch = -89.0f;

	glm::vec3 front;
	front.x = cos(glm::radians(camera.pitch)) * cos(glm::radians(camera.yaw));
	front.y = sin(glm::radians(camera.pitch));
	front.z = cos(glm::radians(camera.pitch)) * sin(glm::radians(camera.yaw));
	camera.cameraFront = glm::normalize(front);
}

int main(int argc, char* argv[]) {
	assert(restart_gl_log());
	/*------------------------------start GL context------------------------------*/
	assert(start_gl());
		
	
	//order to setup things -
	//first create framebuffer
	//then create shaders and setup atributtes and uniforms
	//(view and perspective matrix might be global)
	//then load the objects -> first the obj file, then the mtl file and texture

	//camera = Camera(glm::vec3(0.0, 3.0, 8.0), glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, 1.0, 0.0), 0.0, 0.0, 0.05);
	camera.cameraPosition = glm::vec3(0.0, 4.0, 12.0);
	camera.cameraFront = glm::vec3(0.0, 0.0, -1.0);
	camera.cameraUp = glm::vec3(0.0, 1.0, 0.0);
	camera.cameraSpeed = 0.05;
	camera.pitch = 0.0;
	camera.yaw = 0.0;

	GLSLShader firstPassShader;
	firstPassShader.LoadFromFile(GL_VERTEX_SHADER, "firstPassVS.glsl");
	firstPassShader.LoadFromFile(GL_FRAGMENT_SHADER, "firstPassFS.glsl");
	firstPassShader.CreateAndLinkProgram();
	firstPassShader.Use();
	firstPassShader.AddUniform("viewMatrix");
	firstPassShader.AddUniform("perspectiveMatrix");
	firstPassShader.AddUniform("modelMatrix");
	firstPassShader.AddUniform("reflection");
	firstPassShader.AddUniform("textureIndex");

	//not liking how this is setup (weird order). think something else. -DIEGO
	switch (argc)
	{
	case 1:
		break;
	case 2:
		Scene::instance()->loadScene(argv[1],firstPassShader);//load objects from scene file,passed as argument
		break;
	default:
		return 1;
	}

	firstPassShader.UnUse();

	//load_mtl("testScene.mtl");
	//load_obj("objects.obj", vertexData);
	/*load_texture("cube.png", &firstPassTexture);	
	load_texture("sphere.png", &firstPassTexture);*/
	
	//load_texture("cube.png", &Scene::instance()->objects[0].textureID);
	//load_texture("sphere.png", &Scene::instance()->objects[1].textureID);

	//load_texture("cube.png",&firstTexture);
	//load_texture("sphere.png", &secondTexture);

	createFramebuffer();
	loadSecondPassData();
	createSecondPassShaders();

	glfwSetCursorPosCallback(g_window, mouse_callback);
	/*------------------------------rendering loop--------------------------------*/
	/* some rendering defaults */
	glEnable(GL_DEPTH_TEST); // enable depth-testing

	//glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	//glEnable(GL_CULL_FACE); // cull face
	//glCullFace(GL_BACK); // cull back face
	//glFrontFace(GL_CCW); // GL_CCW for counter clock-wise

	//glEnable(GL_MULTISAMPLE);
	float time = 0;
	GLfloat deltaTime = 0.0f;
	GLfloat lastFrame = 0.0f;
	//change this matrix on on resize callback or change to full screen
	glm::mat4 perspectiveMatrix = glm::perspective(glm::radians(45.0f), (float)g_gl_width / g_gl_height, 0.1f, 100.0f);
	
	//GLuint64 startTime, stopTime;
	//unsigned int queryID[2];

	while (!glfwWindowShouldClose(g_window)) {

		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		camera.cameraSpeed = 5.0f * deltaTime;

		static double previous_seconds = glfwGetTime();
		double current_seconds = glfwGetTime();
		double elapsed_seconds = current_seconds - previous_seconds;
		previous_seconds = current_seconds;

		_update_fps_counter(g_window);
		
		glFinish();
		//glGenQueries(2, queryID);
		//glQueryCounter(queryID[0], GL_TIMESTAMP);
		GLuint query;
		GLuint64 elapsed_time;
		//glGenQueri
		glGenQueries(1, &query);
		glBeginQuery(GL_TIME_ELAPSED, query);

		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		//glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, g_gl_width, g_gl_height);
		glClearColor(0.0, 0.0, 0.0, 1.0);

		//should go in a class (material list?)
		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, firstPassTexture);
		//glBindTexture(GL_TEXTURE_2D, secondTexture);
		//glBindTexture(GL_TEXTURE_2D, firstTexture);
		//glBindTexture(GL_TEXTURE_2D, Scene::instance()->objects[0].textureID);
		time += 0.001; //just to move the camera around
		if (time >= 360)
			time = 0;


		//only one texture array, so only need to bind once
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, Object::globalTextureID);

		glm::mat4 viewMatrix = glm::lookAt(camera.cameraPosition, camera.cameraPosition + camera.cameraFront, camera.cameraUp);
		//rendering code		
		//int index = 0;
		for (auto curObject : Scene::instance()->objects){
			curObject.myShader.Use();	
			//glActiveTexture(GL_TEXTURE0);
			//glBindTexture(GL_TEXTURE_2D_ARRAY, Object::globalTextureID);
			//glBindTexture(GL_TEXTURE_3D, Object::globalTextureID);
			glBindVertexArray(curObject.vertexArrayObject);
			glUniform1f(curObject.myShader("reflection"), curObject.reflection);			
			glUniformMatrix4fv(curObject.myShader("perspectiveMatrix"), 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));			
			glUniformMatrix4fv(curObject.myShader("viewMatrix"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
			glUniformMatrix4fv(curObject.myShader("modelMatrix"), 1, GL_FALSE, glm::value_ptr(curObject.modelMatrix));
			glDrawArrays(GL_TRIANGLES, 0, curObject.vertexData.size());
			curObject.myShader.UnUse();
		}				

		//second pass stuff here
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, frameBufferTexture);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, framBufferNormalTexture);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, frameBufferPositionTexture);

		glActiveTexture(GL_TEXTURE3);
		//glBindTexture(GL_TEXTURE_2D, firstPassTexture);
		glBindTexture(GL_TEXTURE_2D_ARRAY, Object::globalTextureID);

		glUseProgram(secondPassShaderProgram);

		//rebind buffer here and re-submit geometry	

		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, posSSbo);
		//glBufferData(GL_SHADER_STORAGE_BUFFER, (sizeof(vertex)) * BFVertexDataBuffer.size(), NULL, GL_DYNAMIC_COPY);
		//GLint bufMask = GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT;
		//vertex *mapped = reinterpret_cast<vertex *>(glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, (sizeof(vertex)) * BFVertexDataBuffer.size(), bufMask));
		//std::copy(BFVertexDataBuffer.begin(), BFVertexDataBuffer.end(), mapped);
		//glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		//glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, posSSbo);
		//

		glUniform1f(secondPassTimeLocation, time);

		GLuint camPosLoc = glGetUniformLocation(secondPassShaderProgram, "cameraPosition");
		glUniform3f(camPosLoc, camera.cameraPosition.x, camera.cameraPosition.y, camera.cameraPosition.z);

		glBindVertexArray(secondPassVao);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		
		// update other events like input handling 
		glfwPollEvents();


		//move this to input manager
		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(g_window, 1);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_1)){
			glUniform1i(secondPassModeLocation, 1);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_2)){
			glUniform1i(secondPassModeLocation, 2);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_3)){
			glUniform1i(secondPassModeLocation, 3);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_4)){
			glUniform1i(secondPassModeLocation, 4);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_5)){
			glUniform1i(secondPassModeLocation, 5);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_6)){
			glUniform1i(secondPassModeLocation, 6);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_7)){
			glUniform1i(secondPassModeLocation, 7);
		}

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_W))
			camera.cameraPosition += camera.cameraSpeed * camera.cameraFront;

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_S))
			camera.cameraPosition -= camera.cameraSpeed * camera.cameraFront;

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_A))
			camera.cameraPosition -= glm::normalize(glm::cross(camera.cameraFront, camera.cameraUp)) * camera.cameraSpeed;

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_D))
			camera.cameraPosition += glm::normalize(glm::cross(camera.cameraFront, camera.cameraUp)) * camera.cameraSpeed;

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_U))
			camera.cameraPosition.y += camera.cameraSpeed;// glm::normalize(glm::cross(camera.cameraFront, camera.cameraUp)) * camera.cameraSpeed;

		if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_J))
			camera.cameraPosition.y -= camera.cameraSpeed;
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers(g_window);
		
		glFinish();
		//glQueryCounter(queryID[1], GL_TIMESTAMP);
		//GLint stopTimerAvailable = 0;
		//while (!stopTimerAvailable) {
		//	glGetQueryObjectiv(queryID[1],
		//		GL_QUERY_RESULT_AVAILABLE,
		//		&stopTimerAvailable);
		//}
		//glGetQueryObjectui64v(queryID[0], GL_QUERY_RESULT, &startTime);
		//glGetQueryObjectui64v(queryID[1], GL_QUERY_RESULT, &stopTime);

		//printf("Time spent on the GPU: %f ms\n", (stopTime - startTime) / 1000000.0);

		glEndQuery(GL_TIME_ELAPSED);
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &elapsed_time);
		printf("Time spent on the GPU: %f ms\n", (elapsed_time) / 1000000.0);
	}

	// close GL context and any other GLFW resources
	glfwTerminate();
	return 0;
}
