#version 430
//#extension GL_ARB_shader_storage_buffer_object : enable;

layout(location = 0) in vec2 vertex_position;

out vec2 textureCoordinate;

void main() {	
    //+ 1.0 * 0.5 pra centralizar se n�o me engano
	textureCoordinate = (vertex_position + 1.0) * 0.5;
	//textureCoordinate = vertex_position;
	gl_Position = vec4 (vertex_position,0.0, 1.0);
}