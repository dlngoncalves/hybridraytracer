#pragma once

#ifndef OBJECT_H
#define OBJECT_H

#include <glm/glm.hpp>
#include "GLSLShader.h"
#include <vector>
#include <map>

struct vertex{
	glm::vec3 position;
	float padding;
	glm::vec3 normal;
	float padding2;
	glm::vec3 texture;//third float packs what texture we are dealing with!
	float padding3;
};

struct material{
	glm::vec3 ambient;
	float padding;
	glm::vec3 diffuse;
	float padding2;
	glm::vec3 specular;
	float padding3;
	glm::vec3 specTranspText; //packing 3 values below in 1 vec to make passing to gpu easier - will stay this way for now	
	float padding4;
	//float specularExponent;
	//float transparency;
	//int texture;
};

struct Texture{
		GLuint id;
		std::string type;//what would be this?
};

struct face{
	std::string vertexData[3];
};



class Object
{
public:
	//static map<std::string,material> materialList;	
	static vector<material> materialList;
	static GLuint objectCount;
	static GLuint globalTextureID;
	

	GLSLShader &myShader; //one object, one mesh one texture, one shader
	//const Texture &myTexture;  //or pointers here?
	std::vector<vertex> vertexData;
	GLuint vertexBufferObject;
	GLuint vertexArrayObject;
	GLuint textureID;
	GLuint materialID;//if each object has a texture... and a corresponding material? just one ID?
	glm::mat4 modelMatrix;
	//material myMaterial;
	GLfloat reflection;
	std::string objectName;

	void Draw();//maybe not?
	
	Object(const std::string &fileName, GLSLShader &shader,GLfloat reflection)
		: myShader(shader)
	{
		textureID = Object::objectCount;
		Object::objectCount++;
		loadObj(fileName+".obj");
		loadMTL(fileName + ".mtl",reflection);//reflection is both a object and material property
		objectName = fileName;
		setupData();
	};
	
	void loadObj(const std::string &fileName);
	void loadMTL(const std::string &fileName,GLfloat reflection); 
	void loadTexture(const std::string &fileName);
	
	static void loadTextureGlobal(const vector<Object> &objectList);

	void setupData();

	//Object();
	//~Object();
};

//int Object::objectCount = 0;
//void Object::loadObjectsIntoBuffer(vector<vertex> &buffer)
//{
//
//}

//void Object::setupData()
//{
//
//	glGenBuffers(1, &vertexBufferObject);
//	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
//	glBufferData(GL_ARRAY_BUFFER, vertexData.size() * ((3 * sizeof(glm::vec3)) + 3 * sizeof(float)), vertexData.data(), GL_STATIC_DRAW);
//
//
//	glGenVertexArrays(1, &vertexArrayObject);
//	glBindVertexArray(vertexArrayObject);
//	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
//	
//	//might be a problem!
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), 0);
//	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), (GLvoid *)(sizeof(glm::vec3) + sizeof(float)));
//	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), (GLvoid *)(2 * (sizeof(glm::vec3) + sizeof(float))));
//	
//	glEnableVertexAttribArray(0);
//	glEnableVertexAttribArray(1);
//	glEnableVertexAttribArray(2);
//}


//void Object::loadObj(const std::string &fileName)
//{
//	vector<glm::vec3> tempVertices;
//	vector<glm::vec3> tempNormals;
//	vector<glm::vec3> tempTextures;
//	vector<face> tempFaces;
//
//	ifstream in(fileName, ios::in);
//	if (!in){
//		cerr << "Cannot open " << fileName << endl; exit(1);
//	}
//
//	string line;
//	while (getline(in, line))
//	{
//		if (line.substr(0, 2) == "v "){
//			istringstream s(line.substr(2));
//			glm::vec3 v; s >> v.x; s >> v.y; s >> v.z;// v.w = 1.0f;
//			tempVertices.push_back(v);
//		}
//		else if (line.substr(0, 2) == "vn"){
//			istringstream s(line.substr(3));
//			glm::vec3 n; s >> n.x; s >> n.y; s >> n.z;
//			tempNormals.push_back(n);
//		}
//		else if (line.substr(0, 2) == "vt"){
//			istringstream s(line.substr(3));
//			glm::vec3 t; s >> t.s; s >> t.t;
//			tempTextures.push_back(t);
//		}
//		else if (line.substr(0, 2) == "f "){
//			istringstream s(line.substr(2));
//			face newFace;
//
//			s >> newFace.vertexData[0] >> newFace.vertexData[1] >> newFace.vertexData[2];
//			tempFaces.push_back(newFace);
//
//		}
//		else if (line.substr(0, 6) == "mtllib"){
//
//		}
//		else if (line[0] == '#'){/* ignoring this line */}
//		else{/* ignoring this line */}
//	}
//
//	for (auto curFace : tempFaces){
//		vertex newVertex;
//
//		for (int i = 0; i < 3; i++){
//			int firstSlash = curFace.vertexData[i].find_first_of("/");
//			int secondSlash = curFace.vertexData[i].find_last_of("/");
//
//			int vertexIndex = stoi(curFace.vertexData[i].substr(0, firstSlash));
//			newVertex.position = tempVertices[vertexIndex - 1];
//
//			if (tempTextures.size() > 0){
//				//int textureIndex = stoi(curFace.vertexData[i].substr(firstSlash+1, secondSlash-2));
//				//int textureIndex =  stoi(curFace.vertexData[i].substr(0, firstSlash));
//				int textureIndex = stoi(curFace.vertexData[i].substr(firstSlash + 1, secondSlash - firstSlash-1));
//				newVertex.texture = tempTextures[textureIndex - 1];
//			}
//
//			if (tempNormals.size() > 0){
//				int normalIndex = stoi(curFace.vertexData[i].substr(secondSlash + 1));
//				newVertex.normal = tempNormals[normalIndex - 1];
//			}
//
//			vertexData.push_back(newVertex);
//		}
//	}
//
//}

//map<std::string,material> Object::materialList;//should material list be in the scene?
//vector<Object> objectList; //could be a map also?

#endif