#include "Object.h"
#include <iostream>
#include <fstream> 
#include <sstream>  
#include "GLSLShader.h"
#include "stb_image.h"
#include <vector>
#include <map>
////
//
////
GLuint Object::objectCount = 0;
vector<material> Object::materialList;
GLuint Object::globalTextureID;

void Object::setupData()
{

	glGenBuffers(1, &vertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, vertexData.size() * ((3 * sizeof(glm::vec3)) + 3 * sizeof(float)), vertexData.data(), GL_STATIC_DRAW);


	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);

	//might be a problem!
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), (GLvoid *)(sizeof(glm::vec3) + sizeof(float)));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, (3 * sizeof(glm::vec3)) + (3 * sizeof(float)), (GLvoid *)(2 * (sizeof(glm::vec3) + sizeof(float))));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}
//
void Object::loadObj(const std::string &fileName)
{
	vector<glm::vec3> tempVertices;
	vector<glm::vec3> tempNormals;
	vector<glm::vec3> tempTextures;
	vector<face> tempFaces;

	ifstream in(fileName, ios::in);
	if (!in){//THIS SHOULD NOT HAPPEN - JUST IGNORE NOT LOADED FILE!
		cerr << "Cannot open " << fileName << endl; exit(1);
	}

	string line;
	while (getline(in, line))
	{
		if (line.substr(0, 2) == "v "){
			istringstream s(line.substr(2));
			glm::vec3 v; s >> v.x; s >> v.y; s >> v.z;// v.w = 1.0f;
			tempVertices.push_back(v);
		}
		else if (line.substr(0, 2) == "vn"){
			istringstream s(line.substr(3));
			glm::vec3 n; s >> n.x; s >> n.y; s >> n.z;
			tempNormals.push_back(n);
		}
		else if (line.substr(0, 2) == "vt"){
			istringstream s(line.substr(3));
			//glm::vec3 t; s >> t.s; s >> t.t;
			glm::vec3 t; s >> t.x; s >> t.y;
			t.z = textureID;
			tempTextures.push_back(t);
		}
		else if (line.substr(0, 2) == "f "){
			istringstream s(line.substr(2));
			face newFace;

			s >> newFace.vertexData[0] >> newFace.vertexData[1] >> newFace.vertexData[2];
			tempFaces.push_back(newFace);

		}
		else if (line.substr(0, 6) == "mtllib"){

		}
		else if (line[0] == '#'){/* ignoring this line */ }
		else{/* ignoring this line */ }
	}

	for (auto curFace : tempFaces){
		vertex newVertex;

		for (int i = 0; i < 3; i++){
			int firstSlash = curFace.vertexData[i].find_first_of("/");
			int secondSlash = curFace.vertexData[i].find_last_of("/");

			int vertexIndex = stoi(curFace.vertexData[i].substr(0, firstSlash));
			newVertex.position = tempVertices[vertexIndex - 1];

			if (tempTextures.size() > 0){
				//int textureIndex = stoi(curFace.vertexData[i].substr(firstSlash+1, secondSlash-2));
				//int textureIndex =  stoi(curFace.vertexData[i].substr(0, firstSlash));
				int textureIndex = stoi(curFace.vertexData[i].substr(firstSlash + 1, secondSlash - firstSlash - 1));
				newVertex.texture = tempTextures[textureIndex - 1];
			}

			if (tempNormals.size() > 0){
				int normalIndex = stoi(curFace.vertexData[i].substr(secondSlash + 1));
				newVertex.normal = tempNormals[normalIndex - 1];
			}

			vertexData.push_back(newVertex);
		}
	}

}

void Object::loadTexture(const std::string &fileName)
{
	int x, y, n;
	int force_channels = 4;
	unsigned char* image_data = stbi_load(fileName.c_str(), &x, &y, &n, force_channels);
	if (!image_data) {
		fprintf(stderr, "ERROR: could not load %s\n", fileName.c_str());
		return;
	}
	// NPOT check
	if ((x & (x - 1)) != 0 || (y & (y - 1)) != 0) {
		fprintf(
			stderr, "WARNING: texture %s is not power-of-2 dimensions\n", fileName.c_str()
			);
	}
	int width_in_bytes = x * 4;
	unsigned char *top = NULL;
	unsigned char *bottom = NULL;
	unsigned char temp = 0;
	int half_height = y / 2;

	for (int row = 0; row < half_height; row++) {
		top = image_data + row * width_in_bytes;
		bottom = image_data + (y - row - 1) * width_in_bytes;
		for (int col = 0; col < width_in_bytes; col++) {
			temp = *top;
			*top = *bottom;
			*bottom = temp;
			top++;
			bottom++;
		}
	}
	glGenTextures(1, &textureID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RGBA,
		x,
		y,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		image_data
		);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
}

void Object::loadTextureGlobal(const vector<Object> &objectList)
{
	glGenTextures(1, &Object::globalTextureID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, Object::globalTextureID);	
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, 256, 256, objectList.size()); //real bad here	
	
	for (int i = 0; i < objectList.size(); i++){	

		std::string fileName = objectList[i].objectName + ".png";
		int x, y, n;
		int force_channels = 4;
		unsigned char* image_data = stbi_load(fileName.c_str(), &x, &y, &n, force_channels);
		if (!image_data) {
			fprintf(stderr, "ERROR: could not load %s\n", fileName.c_str());
			return;
		}
		// NPOT check
		if ((x & (x - 1)) != 0 || (y & (y - 1)) != 0) {
			fprintf(
				stderr, "WARNING: texture %s is not power-of-2 dimensions\n", fileName.c_str()
				);
		}
		int width_in_bytes = x * 4;
		unsigned char *top = NULL;
		unsigned char *bottom = NULL;
		unsigned char temp = 0;
		int half_height = y / 2;

		for (int row = 0; row < half_height; row++) {
			top = image_data + row * width_in_bytes;
			bottom = image_data + (y - row - 1) * width_in_bytes;
			for (int col = 0; col < width_in_bytes; col++) {
				temp = *top;
				*top = *bottom;
				*bottom = temp;
				top++;
				bottom++;
			}

			
		}
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, x, y, 1, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
	}
	
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}
void Object::loadMTL(const std::string &fileName,GLfloat reflection)
//void Object::loadMTL(const std::string &fileName)
{
	ifstream in(fileName, ios::in);
	if (!in){//THIS SHOULD NOT HAPPEN - JUST IGNORE NOT LOADED FILE!
		cerr << "Cannot open " << fileName << endl; exit(1);
	}

	string line;
	material baseMaterial;

	while (getline(in, line))
	{
		if (line[0] == '#')
			continue;

		if (line.substr(0, 2) == "Ka"){
			istringstream s(line.substr(2));
			s >> baseMaterial.ambient.r; s >> baseMaterial.ambient.g; s >> baseMaterial.ambient.b;
		}

		if (line.substr(0, 2) == "Kd"){
			istringstream s(line.substr(2));
			s >> baseMaterial.diffuse.r; s >> baseMaterial.diffuse.g; s >> baseMaterial.diffuse.b;
		}

		if (line.substr(0, 2) == "Ks"){
			istringstream s(line.substr(2));
			s >> baseMaterial.specular.r; s >> baseMaterial.specular.g; s >> baseMaterial.specular.b;
		}

		if (line.substr(0, 2) == "Ns"){
			istringstream s(line.substr(2));
			s >> baseMaterial.specTranspText.x;
		}

		/*if (line[0] == 'd'){
			istringstream s(line.substr(1));
			s >> baseMaterial.specTranspText.y;
		}*/

		//if (line.substr(0, 3) == "map"){
		//	//do something here -DIEGO
		//	baseMaterial.specTranspText.z = 1;// line.substr(7);
		//}

	}
	baseMaterial.specTranspText.y = reflection;
	Object::materialList.push_back(baseMaterial);
}