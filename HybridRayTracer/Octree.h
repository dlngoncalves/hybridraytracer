#pragma once

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

using namespace std;

struct node{
	node * children[8];
	glm::vec3 center;
	glm::vec3 halfSize;
	int object;
};

class Octree
{
private:

public:
	Octree();
	~Octree();
};

