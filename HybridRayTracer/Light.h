#include <glm/glm.hpp>
#ifndef LIGHT_H
#define LIGHT_H

struct Light
{
	glm::vec3 position;
	float padding1;
	glm::vec3 specular;
	float padding2;
	glm::vec3 diffuse;
	float padding3;
	glm::vec3 ambient;
	float padding4;
};

#endif