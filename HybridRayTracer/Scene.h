#pragma once
#ifndef SCENE_H
#define SCENE_H

#include <string>
#include <vector>
#include "Object.h"
#include "Light.h"
class Scene
{
private:
	static Scene * _instance;	
	std::string sceneName;
	void setupBFVertexDataBuffer();	
	Scene();
public:
	void loadScene(const std::string &sceneFile, GLSLShader &baseShader);
	
	static Scene* instance();
	vector<Object> objects;
	vector<vertex> BFVertexDataBuffer;
	vector<Light> lights;
	~Scene();
};

#endif