#version 430
//#extension GL_ARB_shader_storage_buffer_object : enable;

layout(location=0) in vec3 vertexPosition;
layout(location=1) in vec3 vertexNormal;
layout(location=2) in vec3 vertexUV;//third float packs what texture we are dealing with!

uniform mat4 viewMatrix,perspectiveMatrix,modelMatrix;
uniform float reflection;
//uniform int textureIndex;
out vec4 textureCoordinate;
out vec4 normal;
out vec4 position;

void main(void)
{  
  //position = vec3(modelMatrix * vec4(vertexPosition,1.0));
  //normal = vertexNormal;//vec3(modelMatrix * vec4(vertexNormal,0.0));
  //textureCoordinate = vertexUV.st;
  //gl_Position = perspectiveMatrix * viewMatrix * vec4(position,1.0);

  position = modelMatrix * vec4(vertexPosition,1.0);
  normal = vec4(vertexNormal,reflection);//may be problem  
  textureCoordinate = vec4(vertexUV.stp,1.0);  
  gl_Position = perspectiveMatrix * viewMatrix * position;

 // position = vec3(gl_Position);
}
