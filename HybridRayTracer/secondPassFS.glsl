#version 430
//#extension GL_ARB_shader_storage_buffer_object : enable;

struct vertex{
	vec3 position;
	vec3 normal;
	vec3 texture;// third float serves as a index for both texture and material data
};

struct material{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 specTranspText; //x component is specular power and y component is reflection coefficient (change confusing name later)	
};

struct triangle{
	vec3 v1;
	vec3 v2;
	vec3 v3;
};

struct Ray{
	vec3 origin;
	vec3 direction;
};

struct RayCastHit
{
	float t,u,v;
	vec3 p;
	vec3 normal;
	int objectID;// index of the starting vertex of the hit triangle
};

struct Light
{
	vec3 position;
	vec3 specular;
	vec3 diffuse;	
	vec3 ambient;
};

struct Object{//might not use this
	int index;
	vec3 lowerLeftCorner;
	vec3 upperRightCorner;
};

struct newTriangle{//might not use this
	vertex v1;
	vertex v2;
	vertex v3;	
};

in vec2 textureCoordinate; //coords. for the full screen textures saved in the first pass

layout (binding = 0) uniform sampler2D colorTexture;       //first pass data - texture mapped objects
layout (binding = 1) uniform sampler2D normalTexture;      //first pass data - fragment normal vectors
layout (binding = 2) uniform sampler2D positionTexture;    //first pass data - world position of each fragment
layout (binding = 3) uniform sampler2DArray objectTexture; //Texture array containing all textures from all objects

layout (std430, binding = 4)  buffer bufferObject{
	vertex vertices[];
};

layout (std430, binding = 5)  buffer materialBufferObject{
	material materials[];
};

layout (std430, binding =6) buffer ligthsBuffer{
	Light lightsArray[];
};

#define M_PI 3.1415926
float infinity = 1.0 / 0.0;
uniform int mode;
uniform float time;
uniform vec3 cameraPosition;
uniform int width;
uniform int height;


vec3 point_at_parameter(Ray ray,float t)
{
	return ray.origin + (t* ray.direction);
}

highp float rand(vec2 co)//might not use this
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(co.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}

bool equalsZero(vec3 vector)
{
	if (vector.x < -0.0001 || vector.x > 0.001)
		return false;
	if (vector.y < -0.0001 || vector.y > 0.001)
		return false;
	if (vector.z < -0.0001 || vector.z > 0.001)
		return false;

	return true;
}

bool triangleIntersection(Ray curRay, triangle curTriangle, out RayCastHit hit)
{
	vec3 edge1,edge2;
	vec3 P,Q,T;

	edge1 = curTriangle.v2 - curTriangle.v1;
	edge2 = curTriangle.v3 - curTriangle.v1;

	P = cross(curRay.direction,edge2);

	float det = dot(edge1,P);

	if(abs(det) < 0.00001) return false;

	float inv_det = 1.0/det;

	T = curRay.origin - curTriangle.v1;
	//float u = dot(T,P) * inv_det;
	 hit.u = dot(T,P) * inv_det;
	if(hit.u < 0.0 || hit.u > 1.0) return false;

	Q = cross(T,edge1);
	
	//float v = dot(curRay.direction,Q) * inv_det;
	 hit.v = dot(curRay.direction,Q) * inv_det;
	if(hit.v < 0.0 || hit.u+hit.v > 1.0) return false;

	hit.t = dot(edge2,Q) * inv_det;
	hit.p = point_at_parameter(curRay,hit.t);
	hit.normal = cross(edge1,edge2);
	
	return true;	
};

bool worldHit(Ray ray, float t_min, float t_max, out RayCastHit hit,bool isShadowRay)
{
	RayCastHit tempHit;
	bool anyHit = false;
	float closest = t_max;

	int i = 0;
	while(i < vertices.length()){
		vec3 v1 = vertices[i].position;
		vec3 v2 = vertices[i+1].position;
		vec3 v3 = vertices[i+2].position;
		
		triangle newTriangle = triangle(v1,v2,v3);

		if(triangleIntersection(ray,newTriangle,tempHit)){			
			if(tempHit.t < closest && tempHit.t > t_min){
				anyHit = true;
				closest = tempHit.t;
				hit.t = tempHit.t;
				hit.p = tempHit.p;
				hit.u = tempHit.u;
				hit.v = tempHit.v;				
				//normal vector returned by triangle intersection is uniform to each triangle - so we interpolate from values of vertices normals
				hit.normal = vertices[i].normal + hit.u * (vertices[i+1].normal - vertices[i].normal) + hit.v * (vertices[i+2].normal - vertices[i].normal);
				hit.objectID = i;
				if(isShadowRay){
					return anyHit;
				}
			}
		}
		i+=3;
	}
	return anyHit;
}


vec3 shade(vec3 curCam,  Light curLight, RayCastHit hit, material curMaterial,vec3 textureSample)
{
	
	vec3 ShadowRayOrigin = hit.p + (hit.normal * 0.01);//or 0.001
	vec3 ShadowRayDirection = normalize((curLight.position)-ShadowRayOrigin);
	
	Ray shadowRay = Ray(ShadowRayOrigin,ShadowRayDirection);		
	RayCastHit shadowHit;

	
	//change here to pass texture color as parameter, otherwise reflections will be problematic -DIEGO (but for now ok)
	//ALSO I think texture sampling is a costly operation;
	//I THINK the shadow weirdness happens because shadow testing is being done on a triangle basis- a tri is either in shadow or not?
	//return curLight.ambient * textureSample;
	//return curLight.ambient * vec3(texture(colorTexture,textureCoordinate)) ;
	//returnColor *= 0.3;
	//return vec3(1.0,0.0,0.0);
	//return curLight.ambient * vec3(0.1,0.1,0.1); 	

	//hit.p = shadowHit
	vec3 lightDirection = (curLight.position - hit.p);//something to consider ? I guess ok for point light
	float dist = length(lightDirection);	
	float actualDist = distance(curLight.position,hit.p);
	bool inShadow = worldHit(shadowRay,0.01,actualDist,shadowHit,true);
	if(inShadow)		
		//return vec3(1.0,0.0,0.0);//debug only
		return curLight.ambient * textureSample * curMaterial.ambient; //ambient component may be used to darken shadow parts

	lightDirection = normalize(lightDirection);
	
	

	float diffuse = max(0.0,dot(hit.normal,lightDirection));
	//vec3 Id = curLight.diffuse* sphereArray[hit.objectID].diffuse * diffuse;
	//vec3 Id = curLight.diffuse* vec3(0.5,0.5,0.5) * diffuse;
	float attenuation = 1.0 / (1.0 + 0.1*dist + 0.01*dist*dist);
	diffuse *= attenuation;
	//THIS IS PROBLEMATIC - DIEGO
	//ALSO I THINK TEXTURE SAMPLING OPERATIONS ARE COSTLY!!!
	vec3 Id = curLight.diffuse* textureSample * diffuse * curMaterial.diffuse;//vec3(texture(colorTexture,textureCoordinate)) * diffuse;
	//vec3 Id = curLight.diffuse*  diffuse * curMaterial.diffuse;//vec3(texture(colorTexture,textureCoordinate)) * diffuse;
	//return Id;
	//return curLight.diffuse;
	//return vec3(diffuse,0.0,0.0);

	vec3 pointToViewer = normalize(curCam - hit.p);
	vec3 reflection = reflect(-lightDirection,hit.normal);
	float specular = max(0.0, dot(reflection,pointToViewer));

	//specular = pow(specular,sphereArray[hit.objectID].power);
	specular = pow(specular,curMaterial.specTranspText.x);
	
	//vec3 Is = curLight.specular * sphereArray[hit.objectID].specular * specular;
	vec3 Is = curLight.specular * curMaterial.specular * specular;//making things less shiny!
	//vec3 Is = curLight.specular * vec3(1.0,1.0,1.0) * specular;

	//vec3 Ia = curLight.ambient * sphereArray[hit.objectID].ambient;
	//vec3 Ia = curLight.ambient * vec3(0.1,0.1,0.1);
	vec3 Ia;
	//if(mode == 1)
		//Ia = curLight.ambient * curMaterial.ambient * textureSample;//vec3(texture(colorTexture,textureCoordinate));
	//if(mode == 2)
		Ia = curLight.ambient *  textureSample;//vec3(texture(colorTexture,textureCoordinate));

	//hit = shadowHit;
	vec3 returnColor = Id+Is+Ia;

	return returnColor;
}

//vec3 getColor(Ray curRay,vec3 textureSample,out RayCastHit hit,material curMaterial){	
vec3 getColor(vec3 position,vec3 textureSample,out RayCastHit hit,material curMaterial){
	vec3 finalColor;
	for(int i = 0;i < lightsArray.length(); i++){			
		Light newTestLight = lightsArray[i];//for some reason this is the only way it works
		finalColor += shade(position,newTestLight,hit,curMaterial,textureSample);
	}
	return finalColor;
}

vec3 getTextureValue(float u, float v, int textureId)
{
					
	vec3 t1 = vertices[textureId].texture;
	vec3 t2 = vertices[textureId+1].texture;
	vec3 t3 = vertices[textureId+2].texture;
	float s = t1.s + u*(t2.s -t1.s) + v*(t3.s - t1.s);
	float t = t1.t + u*(t2.t -t1.t) + v*(t3.t - t1.t);
	vec3 textureHit = vec3(s,t,vertices[textureId].texture.p);//p is the texture index

	return vec3(texture(objectTexture,textureHit));	
};

out vec4 out_Color;

void main(void)
{
	//get data from first pass
	vec3 position = vec3(texture(positionTexture,textureCoordinate));
	vec4 nData = texture(normalTexture,textureCoordinate);
	vec3 normal = nData.rgb;
	float reflection = nData.a;
	
	vec4 colorData = (texture(colorTexture,textureCoordinate));
	vec3 color = colorData.rgb;//vec3(texture(colorTexture,textureCoordinate));
	float id = colorData.a;
	int matId = int(id);
	material curMaterial = materials[matId];
	
	vec3 finalColor = vec3(0.0,0.0,0.0);
	//now trace secundary rays based on these values 
	if(mode == 1){	
		if(!equalsZero(normal)){
	
		
			RayCastHit hit;
			hit.p = position;
			hit.normal = normalize(normal);				
			vec3 textureSample = vec3(texture(colorTexture,textureCoordinate));			
			int depth = 5;
			float frac = 1.0;
			vec3 eyePos = cameraPosition;
			for(int i = 0; i < depth; i++){
				vec3 localColor = vec3(0.0);
				for(int i = 0;i < lightsArray.length(); i++){			
					Light newTestLigth = lightsArray[i];//for some reason this is the only way it works
					//localColor += shade(eyePos,newTestLigth,hit,curMaterial,textureSample);//problem? -DIEGO
					localColor += shade(cameraPosition,newTestLigth,hit,curMaterial,textureSample);//problem? -DIEGO
				}

				finalColor += localColor*(1.0-reflection)*frac;
				
				if(reflection < 0.01)
					break;
				
				//now setup reflection stuff
				vec3 ReflectRayOrigin = hit.p + (hit.normal*0.001);
				vec3 camRay = (ReflectRayOrigin - eyePos);
				vec3 ReflectRayDirection = reflect(camRay,hit.normal);
				
				Ray reflectionRay = Ray(ReflectRayOrigin,ReflectRayDirection);
				
				if(!worldHit(reflectionRay,0.01,infinity,hit,false))//{
					break;
						
				int id = hit.objectID;
				matId = int(vertices[id].texture.p);
				curMaterial = materials[matId];
				textureSample = getTextureValue(hit.u,hit.v,id);
				eyePos = ReflectRayOrigin;
				reflection = curMaterial.specTranspText.y;
				
				//if(mode==2)
					//frac *= reflection;
				//ReflectRayOrigin = reflectHit.p + (reflectHit.normal*0.001);

				//ReflectRayDirection = reflect(reflectionRay.direction,reflectHit.normal);
				//reflectionRay.origin = ReflectRayOrigin;
				//reflectionRay.direction = ReflectRayDirection;
			}
			//here world hit -> if it doesn hit, also break; if it hits, setup stuff and calculate color with for loop above
				
			//}
			///coment everything down here
			//for(int i = 0;i < lightsArray.length(); i++){			
			//	Light newTestLigth = lightsArray[i];//for some reason this is the only way it works
			//	finalColor += shade(cameraPosition,newTestLigth,hit,curMaterial,textureSample);
			//}
			////finalColor /= lightsArray.length();
			
			//vec3 reflectColor = vec3(0.0,0.0,0.0);
			//vec3 tempColor = vec3(0.0,0.0,0.0);
			//vec3 ReflectRayOrigin = hit.p + (hit.normal*0.001);		
			//vec3 camRay = (ReflectRayOrigin - cameraPosition);		
			//vec3 ReflectRayDirection = reflect(camRay,hit.normal);

			//Ray reflectionRay = Ray(ReflectRayOrigin,ReflectRayDirection);
			//RayCastHit reflectHit;				
			

			//if(reflection > 0.01){
			//	int depth = 1;
			//	for(int i = 0; i < depth; i++){
			//		if(worldHit(reflectionRay,0.01,infinity,reflectHit,false)){
			//			int id = reflectHit.objectID;
			//			matId = int(vertices[id].texture.p);
			//			curMaterial = materials[matId];
			//			textureSample = getTextureValue(reflectHit.u,reflectHit.v,id);
			//			for(int i = 0;i < lightsArray.length(); i++){							
			//				Light newTestLight = lightsArray[i];
			//				if(mode == 1)
			//					reflectColor += shade(cameraPosition,newTestLight,reflectHit,curMaterial,textureSample);
			//				if(mode == 2)//why is this working the way it is?
			//					reflectColor += shade(ReflectRayOrigin,newTestLight,reflectHit,curMaterial,textureSample);
			//			}

			//			//reflection = curMaterial.specTranspText.y;
			//			//done calculating shading here
			//			////reflectColor /= lightsArray.length();

			//			//if(mode == 3)
			//				//ReflectRayOrigin = reflectHit.p + (hit.normal*0.001);
			//			//else if(mode == 2)
			//				ReflectRayOrigin = reflectHit.p + (reflectHit.normal*0.001);

			//			ReflectRayDirection = reflect(reflectionRay.direction,reflectHit.normal);
			//			reflectionRay.origin = ReflectRayOrigin;
			//			reflectionRay.direction = ReflectRayDirection;

	
			//			//vec3 texColor = getTextureValue(reflectHit.u,reflectHit.v,id);
			//			//reflectColor += texColor * ((depth-i)/depth*2);

			//		}
   // 				else{//work some basic sky reflection here					
			//			//vec3 unitDirection = normalize(reflectionRay.direction);
			//			//float t = 0.5 * unitDirection.y + 1.0;
			//			//reflectColor *=  (1.0-t) * vec3(1.0,1.0,1.0) + t * vec3(0.5,0.7,1.0);
			//		}

			//	}
			//	finalColor *= (1-reflection);
			//	reflectColor *= reflection;
			//	finalColor += reflectColor;
			//} 
			out_Color = vec4(finalColor,1.0);  
		}//equals zero
		else{//basic interpolated color for "sky"
			//float u = gl_FragCoord.x/800;
			//float v = gl_FragCoord.y/500;
			//float t = 0.5 * (v + 1.0);
			//vec3 color = (1.0-t) * vec3(1.0,1.0,1.0) + t * vec3(0.5,0.7,1.0);
			//out_Color = vec4(color,1.0);
		}
	}

	//DEBUG MODES
	if(mode ==2)
		out_Color = vec4(color,1.0);		
	if(mode == 3)
		out_Color = vec4(normal,1.0);  	
	if(mode ==4){
		out_Color = vec4(position,1.0);		
		float texid = id/3;
		out_Color = vec4(texid,texid,texid,1.0);
		//out_Color = vec4(colorTexture.s,0.0,0.0,1.0);		
	}
	if(mode ==5){						
		if(equalsZero(normal)){//melhorar a visualização - usar no trabalho
			reflection = 0.0;
		}else if(reflection >0.0){		
			out_Color = vec4(reflection,0.0,0.0,1.0);
		}else{
			out_Color = vec4(0.1,0.1,0.1,1.0);
		}

	}

}